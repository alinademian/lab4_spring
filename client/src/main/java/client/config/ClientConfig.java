package client.config;

import client.service.LabProblemServiceClient;
import client.service.StudentServiceClient;
import client.ui.ClientConsole;
import common.service.Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@Configuration
public class ClientConfig {

    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean() {
        RmiProxyFactoryBean rmiProxyFactoryBean = new RmiProxyFactoryBean();

        rmiProxyFactoryBean.setServiceInterface(Service.class);
        rmiProxyFactoryBean.setServiceUrl("rmi://localhost:1099/StudentService");

        return rmiProxyFactoryBean;
    }

    @Bean
    RmiProxyFactoryBean rmiProxyFactoryBean2() {
        RmiProxyFactoryBean rmiProxyFactoryBean2 = new RmiProxyFactoryBean();

        rmiProxyFactoryBean2.setServiceInterface(Service.class);
        rmiProxyFactoryBean2.setServiceUrl("rmi://localhost:1099/LabProblemService");

        return rmiProxyFactoryBean2;
    }

    @Bean
    ClientConsole clientConsole() {
        return new ClientConsole(studentServiceClient(), labProblemServiceClient());
    }

    @Bean
    StudentServiceClient studentServiceClient() {
        return new StudentServiceClient();
    }

    @Bean
    LabProblemServiceClient labProblemServiceClient() {
        return new LabProblemServiceClient();
    }
}
