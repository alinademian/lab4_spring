package client.service;

import common.domain.LabProblem;
import common.repository.Repository;
import common.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

//@Component("LabProblemService")
public class LabProblemServiceClient implements Service<Long, LabProblem> {

    @Autowired
    private Service<Long, LabProblem> service;


    public void add(LabProblem labProblem) {
        service.add(labProblem);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public void update(LabProblem labProblem) {
        service.update(labProblem);
    }

    public List<LabProblem> getAll() {
        return service.getAll();
    }
}
