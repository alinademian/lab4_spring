package client.service;

import common.repository.Repository;
import common.domain.Student;
import common.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.List;

//@Primary
//@Component("StudentService")
public class StudentServiceClient implements Service<Long, Student> {

//    @Autowired
//    private Repository<Long, Student> studentServiceClient;

    @Autowired
    private Service<Long, Student> service;


    public void add(Student student) {
        service.add(student);
    }

    public void delete(Long id) {
        service.delete(id);
    }

    public void update(Student student) {
        service.update(student);
    }

    public List<Student> getAll() {
        return service.getAll();
    }
}
