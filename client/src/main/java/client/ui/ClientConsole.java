package client.ui;

import client.service.LabProblemServiceClient;
import client.service.StudentServiceClient;
import common.domain.LabProblem;
import common.domain.Student;
import common.service.Service;

import java.util.List;
import java.util.Scanner;

public class ClientConsole {
    private Scanner scanner;


//    @Autowired
//    private Service<Long, Student> studentService;

    private Service<Long, Student> studentService;

    private Service<Long, LabProblem> labProblemService;

    public ClientConsole(StudentServiceClient studentService, LabProblemServiceClient labProblemService) {
        scanner = new Scanner(System.in);
        this.studentService = studentService;
        this.labProblemService = labProblemService;
    }

    public void runConsole() {

        printMenu();

        while (true) {

            String option = scanner.next();
            if (option.equals("x")) {
                break;
            }
            switch (option) {
                case "1":
                    addStudent();
                    break;
                case "2":
                    removeStudent();
                    break;
                case "3":
                    updateStudent();
                    break;
                case "4":
                    printStudents();
                    break;
                case "5":
                    addLabProblem();
                    break;
                case "6":
                    removeLabProblem();
                    break;
                case "7":
                    updateLabProblem();
                    break;
                case "8":
                    printLabProblems();
                    break;
                default:
                    System.out.println("Not Yet Implemented");
            }
            printMenu();
        }
    }

    private void addStudent() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        System.out.println("Name: ");
        String name = scanner.next();

        System.out.println("Group: ");
        int group = scanner.nextInt();

        Student student = new Student(id, name, group);

        studentService.add(student);

    }

    private void removeStudent() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        studentService.delete(id);

    }

    private void updateStudent() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        System.out.println("Noul nume: ");
        String name = scanner.next();

        System.out.println("Noua grupa: ");
        Integer group = scanner.nextInt();

        Student newStudent = new Student(id, name, group);

        studentService.update(newStudent);

    }

    private void printStudents() {

        System.out.println("All students: \n");
        List<Student> students = studentService.getAll();
        students.forEach(System.out::println);
    }

    private void addLabProblem() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        System.out.println("Discipline: ");
        String discipline = scanner.next();

        System.out.println("Lab number: ");
        int labNumber = scanner.nextInt();

        LabProblem labProblem = new LabProblem(id, discipline, labNumber);

        labProblemService.add(labProblem);

    }

    private void removeLabProblem() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        labProblemService.delete(id);

    }

    private void updateLabProblem() {

        System.out.println("Id: ");
        Long id = scanner.nextLong();

        System.out.println("Noua disciplina: ");
        String discipline = scanner.next();

        System.out.println("Noul lab number: ");
        Integer labNumber = scanner.nextInt();

        LabProblem labProblem = new LabProblem(id, discipline, labNumber);

        labProblemService.update(labProblem);

    }

    private void printLabProblems() {

        System.out.println("All lab problems: \n");
        List<LabProblem> labProblems = labProblemService.getAll();
        labProblems.forEach(System.out::println);
    }

    private void printMenu() {
        System.out.println(
                "1 - Add Student\n" +
                "2 - Remove Student\n" +
                "3 - Update Student\n" +
                "4 - Print all Students\n" +
                "♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡\n" +
                "5 - Add Lab Problem\n" +
                "6 - Remove Lab Problem\n" +
                "7 - Update Lab Problem\n" +
                "8 - Print all Lab Problems\n" +
                "♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡\n" +
                "x - Exit\n" +
                "☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠☠\n");
    }
}
