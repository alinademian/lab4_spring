package server.config;

import common.domain.LabProblem;
import common.repository.Repository;
import common.service.Service;
import common.domain.Student;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import server.repository.LabProblemRepoImpl;
import server.repository.StudentRepoImpl;
import server.service.LabProblemServiceImpl;
import server.service.StudentServiceImpl;

@Configuration
public class ServerConfig {

    @Bean
    RmiServiceExporter rmiServiceExporter() {
        RmiServiceExporter rmiServiceExporter = new RmiServiceExporter();

        rmiServiceExporter.setServiceInterface(Service.class);
        rmiServiceExporter.setServiceName("StudentService");
        rmiServiceExporter.setService(studentService());

        return rmiServiceExporter;
    }

    @Bean
    RmiServiceExporter rmiServiceExporter2() {
        RmiServiceExporter rmiServiceExporter2 = new RmiServiceExporter();

        rmiServiceExporter2.setServiceInterface(Service.class);
        rmiServiceExporter2.setServiceName("LabProblemService");
        rmiServiceExporter2.setService(labProblemService());

        return rmiServiceExporter2;
    }

    @Bean
    Service<Long, Student> studentService() {
        return new StudentServiceImpl(studentRepository());
    }

    @Bean
    Service<Long, LabProblem> labProblemService() {
        return new LabProblemServiceImpl(labProblemRepository());
    }

    @Bean
    Repository<Long, Student> studentRepository() {
        return new StudentRepoImpl();
    }

    @Bean
    Repository<Long, LabProblem> labProblemRepository() {
        return new LabProblemRepoImpl();
    }
}
