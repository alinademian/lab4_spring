package server.repository;

import common.repository.Repository;
import common.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.List;

public class StudentRepoImpl implements Repository<Long, Student> {

    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public void add(Student student) {
        String sql = "INSERT INTO student (name, groupnumber) VALUES (?, ?)";
        jdbcOperations.update(sql, student.getName(), student.getGroup());
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM student WHERE id=?";
        jdbcOperations.update(sql, id);
    }

    @Override
    public void update(Student student) {
        String sql = "UPDATE student SET name=?, groupnumber=? WHERE id=?";
        jdbcOperations.update(sql, student.getName(), student.getGroup(),
                student.getId());
    }

    @Override
    public List<Student> getAll() {
        String sql = "SELECT * FROM student";

        return jdbcOperations.query(sql, (rs, i) -> {
            Student student = new Student();
            student.setId(rs.getLong("id"));
            student.setName(rs.getString("name"));
            student.setGroup(rs.getInt("groupnumber"));
            return student;
        });
    }
}
