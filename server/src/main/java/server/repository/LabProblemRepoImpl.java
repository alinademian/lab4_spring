package server.repository;

import common.domain.LabProblem;
import common.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

import java.util.List;

public class LabProblemRepoImpl implements Repository<Long, LabProblem> {

    @Autowired
    private JdbcOperations jdbcOperations;

    @Override
    public void add(LabProblem labProblem) {
        String sql = "INSERT INTO labproblem (discipline, labnumber) VALUES (?,?)";
        jdbcOperations.update(sql, labProblem.getDiscipline(), labProblem.getLabNumber());
    }

    @Override
    public void delete(Long id) {
        String sql = "DELETE FROM labproblem WHERE id=?";
        jdbcOperations.update(sql, id);
    }

    @Override
    public void update(LabProblem labProblem) {
        String sql = "UPDATE labproblem SET discipline=?, labnumber=? WHERE id=?";
        jdbcOperations.update(sql, labProblem.getDiscipline(), labProblem.getLabNumber(), labProblem.getId());
    }

    @Override
    public List<LabProblem> getAll() {
        String sql = "SELECT * FROM labproblem";

        return jdbcOperations.query(sql, (rs, i) -> {
            LabProblem labProblem = new LabProblem();
            labProblem.setId(rs.getLong("id"));
            labProblem.setDiscipline(rs.getString("discipline"));
            labProblem.setLabNumber(rs.getInt("labnumber"));
            return labProblem;
        });
    }
}
