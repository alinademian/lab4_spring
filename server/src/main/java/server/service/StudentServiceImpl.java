package server.service;

import common.service.Service;
import common.domain.Student;
import common.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements Service<Long, Student> {

    private Repository<Long, Student> studentRepository;

    public StudentServiceImpl(Repository<Long, Student> studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void add(Student student) {
        this.studentRepository.add(student);
    }

    @Override
    public void delete(Long id) {
        this.studentRepository.delete(id);
    }

    @Override
    public void update(Student student) {
        this.studentRepository.update(student);
    }

    @Override
    public List<Student> getAll() {
        List<Student> students = new ArrayList<>();
        this.studentRepository.getAll().forEach(students::add);

        return students;
    }
}
