package server.service;

import common.domain.LabProblem;
import common.repository.Repository;
import common.service.Service;

import java.util.ArrayList;
import java.util.List;

public class LabProblemServiceImpl implements Service<Long, LabProblem> {

    private Repository<Long, LabProblem> labProblemRepository;

    public LabProblemServiceImpl(Repository<Long, LabProblem> labProblemRepository) {
        this.labProblemRepository = labProblemRepository;
    }

    @Override
    public void add(LabProblem labProblem) {
        this.labProblemRepository.add(labProblem);
    }

    @Override
    public void delete(Long id) {
        this.labProblemRepository.delete(id);
    }

    @Override
    public void update(LabProblem labProblem) {
        this.labProblemRepository.update(labProblem);
    }

    @Override
    public List<LabProblem> getAll() {
        List<LabProblem> labProblems = new ArrayList<>();
        this.labProblemRepository.getAll().forEach(labProblems::add);

        return labProblems;
    }
}
