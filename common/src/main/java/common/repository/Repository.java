package common.repository;

import common.domain.Entity;

import java.io.Serializable;
import java.util.List;

public interface Repository <ID extends Serializable, T extends Entity<ID>> {

    void add(T entity);
    void delete(ID id);
    void update(T entity);
    List<T> getAll();
    
    
}
