package common.service;

import common.domain.Entity;

import java.io.Serializable;
import java.util.List;

public interface Service<ID extends Serializable, T extends Entity<ID>> {
    void add(T entity);
    void delete(ID id);
    void update(T entity);
    List<T> getAll();
}
